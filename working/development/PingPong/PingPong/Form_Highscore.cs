﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace PingPong
{
    public partial class Form_Highscore : Form
    {
        public Form_Highscore()
        {
            InitializeComponent();
            fillform(filepath);
        }
        public int counter;
        public string line;
        public string filepath="Highscore.txt";
       
        public void fillform (string filepath)
        {
            if (File.Exists(filepath))
            { 
            int y = 100;
            StreamReader sr = new StreamReader(filepath);
                while (sr.Peek() != -1)
                {
                    string[] line = (sr.ReadLine()).Split(';');

                    Label l1 = new Label();
                    l1.Text = line[0];
                    l1.Location = new Point(54, y);

                    Label l2 = new Label();
                    l2.Text = line[1];
                    l2.Location = new Point(178, y);

                    this.Controls.Add(l1);
                    this.Controls.Add(l2);

                    y += 50;

                }
                sr.Close();
            }
            
        }
    }
}
