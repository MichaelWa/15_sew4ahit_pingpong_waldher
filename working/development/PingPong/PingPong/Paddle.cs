﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingPong
{
    public class Paddle : IPaddleandBall
    {
        public Point location;
        public Size size;
        public bool moveUp, moveDown;
        public int Score;

        public Paddle()
        {
            Score = 0;
            size.Width = 35;
            size.Height = 200;
        }
    }
}
