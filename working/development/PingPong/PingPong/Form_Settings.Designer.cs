﻿namespace PingPong
{
    partial class Form_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bSave = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.bDefault = new System.Windows.Forms.Button();
            this.bBackgC = new System.Windows.Forms.Button();
            this.bPlayerC = new System.Windows.Forms.Button();
            this.bBallC = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rbDefault = new System.Windows.Forms.RadioButton();
            this.rbmx2 = new System.Windows.Forms.RadioButton();
            this.rbmx3 = new System.Windows.Forms.RadioButton();
            this.rbmx4 = new System.Windows.Forms.RadioButton();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.colorDialog2 = new System.Windows.Forms.ColorDialog();
            this.colorDialog3 = new System.Windows.Forms.ColorDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bSave
            // 
            this.bSave.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.bSave.Location = new System.Drawing.Point(44, 286);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(75, 23);
            this.bSave.TabIndex = 0;
            this.bSave.Text = "SAVE";
            this.bSave.UseVisualStyleBackColor = false;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // bCancel
            // 
            this.bCancel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.bCancel.Location = new System.Drawing.Point(167, 286);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 1;
            this.bCancel.Text = "CANCEL";
            this.bCancel.UseVisualStyleBackColor = false;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bDefault
            // 
            this.bDefault.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.bDefault.Location = new System.Drawing.Point(287, 286);
            this.bDefault.Name = "bDefault";
            this.bDefault.Size = new System.Drawing.Size(75, 23);
            this.bDefault.TabIndex = 2;
            this.bDefault.Text = "DEFAULT";
            this.bDefault.UseVisualStyleBackColor = false;
            this.bDefault.Click += new System.EventHandler(this.bDefault_Click);
            // 
            // bBackgC
            // 
            this.bBackgC.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.bBackgC.Location = new System.Drawing.Point(50, 55);
            this.bBackgC.Name = "bBackgC";
            this.bBackgC.Size = new System.Drawing.Size(75, 23);
            this.bBackgC.TabIndex = 3;
            this.bBackgC.Text = "CHANGE";
            this.bBackgC.UseVisualStyleBackColor = false;
            this.bBackgC.Click += new System.EventHandler(this.bBackgC_Click);
            // 
            // bPlayerC
            // 
            this.bPlayerC.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.bPlayerC.Location = new System.Drawing.Point(385, 55);
            this.bPlayerC.Name = "bPlayerC";
            this.bPlayerC.Size = new System.Drawing.Size(75, 23);
            this.bPlayerC.TabIndex = 4;
            this.bPlayerC.Text = "CHANGE";
            this.bPlayerC.UseVisualStyleBackColor = false;
            this.bPlayerC.Click += new System.EventHandler(this.bPlayerC_Click);
            // 
            // bBallC
            // 
            this.bBallC.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.bBallC.Location = new System.Drawing.Point(44, 170);
            this.bBallC.Name = "bBallC";
            this.bBallC.Size = new System.Drawing.Size(75, 23);
            this.bBallC.TabIndex = 5;
            this.bBallC.Text = "CHANGE";
            this.bBallC.UseVisualStyleBackColor = false;
            this.bBallC.Click += new System.EventHandler(this.bBallC_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "BACKGROUND-COLOR";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(385, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "PLAYER-COLOR";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "BALL-COLOR";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(385, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "GAME-SPEED";
            // 
            // rbDefault
            // 
            this.rbDefault.AutoSize = true;
            this.rbDefault.Checked = true;
            this.rbDefault.Location = new System.Drawing.Point(385, 176);
            this.rbDefault.Name = "rbDefault";
            this.rbDefault.Size = new System.Drawing.Size(74, 17);
            this.rbDefault.TabIndex = 10;
            this.rbDefault.TabStop = true;
            this.rbDefault.Text = "DEFAULT";
            this.rbDefault.UseVisualStyleBackColor = true;
            this.rbDefault.Click += new System.EventHandler(this.rbDefault_Click);
            // 
            // rbmx2
            // 
            this.rbmx2.AutoSize = true;
            this.rbmx2.Location = new System.Drawing.Point(385, 199);
            this.rbmx2.Name = "rbmx2";
            this.rbmx2.Size = new System.Drawing.Size(103, 17);
            this.rbmx2.TabIndex = 11;
            this.rbmx2.Text = "MULTIPLIER x2";
            this.rbmx2.UseVisualStyleBackColor = true;
            this.rbmx2.CheckedChanged += new System.EventHandler(this.rbmx2_CheckedChanged);
            this.rbmx2.Click += new System.EventHandler(this.rbmx2_Click);
            // 
            // rbmx3
            // 
            this.rbmx3.AutoSize = true;
            this.rbmx3.Location = new System.Drawing.Point(385, 222);
            this.rbmx3.Name = "rbmx3";
            this.rbmx3.Size = new System.Drawing.Size(103, 17);
            this.rbmx3.TabIndex = 12;
            this.rbmx3.Text = "MULTIPLIER x3";
            this.rbmx3.UseVisualStyleBackColor = true;
            this.rbmx3.CheckedChanged += new System.EventHandler(this.rbmx3_CheckedChanged);
            this.rbmx3.Click += new System.EventHandler(this.rbmx3_Click);
            // 
            // rbmx4
            // 
            this.rbmx4.AutoSize = true;
            this.rbmx4.Location = new System.Drawing.Point(385, 245);
            this.rbmx4.Name = "rbmx4";
            this.rbmx4.Size = new System.Drawing.Size(103, 17);
            this.rbmx4.TabIndex = 13;
            this.rbmx4.Text = "MULTIPLIER x4";
            this.rbmx4.UseVisualStyleBackColor = true;
            this.rbmx4.CheckedChanged += new System.EventHandler(this.rbmx4_CheckedChanged);
            this.rbmx4.Click += new System.EventHandler(this.rbmx4_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.Location = new System.Drawing.Point(141, 55);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(26, 23);
            this.panel1.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel2.Location = new System.Drawing.Point(141, 170);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(26, 23);
            this.panel2.TabIndex = 15;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel3.Location = new System.Drawing.Point(482, 55);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(26, 23);
            this.panel3.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(385, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "label5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(422, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "ms Intervalle";
            // 
            // Form_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 321);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.rbmx4);
            this.Controls.Add(this.rbmx3);
            this.Controls.Add(this.rbmx2);
            this.Controls.Add(this.rbDefault);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bBallC);
            this.Controls.Add(this.bPlayerC);
            this.Controls.Add(this.bBackgC);
            this.Controls.Add(this.bDefault);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bSave);
            this.Name = "Form_Settings";
            this.Text = "Form_Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bDefault;
        private System.Windows.Forms.Button bBackgC;
        private System.Windows.Forms.Button bPlayerC;
        private System.Windows.Forms.Button bBallC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rbDefault;
        private System.Windows.Forms.RadioButton rbmx2;
        private System.Windows.Forms.RadioButton rbmx3;
        private System.Windows.Forms.RadioButton rbmx4;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ColorDialog colorDialog2;
        private System.Windows.Forms.ColorDialog colorDialog3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}