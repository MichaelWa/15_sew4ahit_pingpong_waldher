﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace PingPong
{
    public partial class Form_Settings : Form
    {
        string conStr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=mydb.accdb;";
        OleDbConnection con;
        public Form_Settings()
        {
            InitializeComponent();
            con = new OleDbConnection(conStr);
            start();
        }

        private void start()
        {
            con.Open();
            OleDbCommand cmd1 = new OleDbCommand();
            OleDbCommand cmd2 = new OleDbCommand();
            OleDbCommand cmd3 = new OleDbCommand();
            OleDbCommand cmd4 = new OleDbCommand();

            OleDbCommand cmd5 = new OleDbCommand();
            OleDbCommand cmd6 = new OleDbCommand();
            OleDbCommand cmd7 = new OleDbCommand();
            OleDbCommand cmd8 = new OleDbCommand();

            OleDbCommand cmd9 = new OleDbCommand();
            OleDbCommand cmd10 = new OleDbCommand();
            OleDbCommand cmd11 = new OleDbCommand();
            OleDbCommand cmd12 = new OleDbCommand();

            OleDbCommand cmd13 = new OleDbCommand();

            cmd1.Connection = con;
            cmd2.Connection = con;
            cmd3.Connection = con;
            cmd4.Connection = con;

            cmd5.Connection = con;
            cmd6.Connection = con;
            cmd7.Connection = con;
            cmd8.Connection = con;

            cmd9.Connection = con;
            cmd10.Connection = con;
            cmd11.Connection = con;
            cmd12.Connection = con;

            cmd13.Connection = con;

            cmd1.CommandText = "select BackgA from mySettings";
            cmd2.CommandText = "select BackgR from mySettings";
            cmd3.CommandText = "select BackgG from mySettings";
            cmd4.CommandText = "select BackgB from mySettings";

            cmd5.CommandText = "select BallA from mySettings";
            cmd6.CommandText = "select BallR from mySettings";
            cmd7.CommandText = "select BallG from mySettings";
            cmd8.CommandText = "select BallB from mySettings";

            cmd9.CommandText = "select PlayerA from mySettings";
            cmd10.CommandText = "select PlayerR from mySettings";
            cmd11.CommandText = "select PlayerG from mySettings";
            cmd12.CommandText = "select PlayerB from mySettings";

            cmd13.CommandText = "select Speed from mySettings";

            try
            {
                int BackA = (int)cmd1.ExecuteScalar();
                int BackR = (int)cmd2.ExecuteScalar();
                int BackG = (int)cmd3.ExecuteScalar();
                int BackB = (int)cmd4.ExecuteScalar();

                int BallA = (int)cmd5.ExecuteScalar();
                int BallR = (int)cmd6.ExecuteScalar();
                int BallG = (int)cmd7.ExecuteScalar();
                int BallB = (int)cmd8.ExecuteScalar();

                int PlayerA = (int)cmd9.ExecuteScalar();
                int PlayerR = (int)cmd10.ExecuteScalar();
                int PlayerG = (int)cmd11.ExecuteScalar();
                int PlayerB = (int)cmd12.ExecuteScalar();

                int Speed = (int)cmd13.ExecuteScalar();

                panel1.BackColor = Color.FromArgb(BackA, BackR, BackG, BackB);
                panel2.BackColor = Color.FromArgb(BallA, BallR, BallG, BallB);
                panel3.BackColor = Color.FromArgb(PlayerA, PlayerR, PlayerG, PlayerB);
                label5.Text = Convert.ToString(Speed);

                if (Speed == 100)
                    rbDefault.Select();
                if (Speed == 50)
                    rbmx2.Select();
                if (Speed == 33)
                    rbmx3.Select();
                if (Speed == 25)
                    rbmx4.Select();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            con.Close();
        
    }
        private void bBackgC_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                panel1.BackColor = colorDialog1.Color;
            }
        }

        private void bBallC_Click(object sender, EventArgs e)
        {
            if (colorDialog2.ShowDialog() == DialogResult.OK)
            {
                panel2.BackColor = colorDialog2.Color;
            }
            
        }

        private void bPlayerC_Click(object sender, EventArgs e)
        {
            if (colorDialog3.ShowDialog() == DialogResult.OK)
            {
                panel3.BackColor = colorDialog3.Color;
            }
            
        }
       

        private void rbDefault_Click(object sender, EventArgs e)
        {
            label5.Text = "100";
        }
        private void rbmx2_Click(object sender, EventArgs e)
        {
            label5.Text = "50";
        }
        private void rbmx2_CheckedChanged(object sender, EventArgs e)
        {
            
        }
        private void rbmx3_Click(object sender, EventArgs e)
        {
            label5.Text = "33";
        }
        private void rbmx3_CheckedChanged(object sender, EventArgs e)
        {
            
        }
        private void rbmx4_Click(object sender, EventArgs e)
        {
            label5.Text = "25";
        }
        private void rbmx4_CheckedChanged(object sender, EventArgs e)
        {
           
        }
        private void bDefault_Click(object sender, EventArgs e)
        {
            con.Open();
            OleDbCommand cmd1 = new OleDbCommand();
            OleDbCommand cmd2 = new OleDbCommand();
            OleDbCommand cmd3 = new OleDbCommand();
            OleDbCommand cmd4 = new OleDbCommand();

            OleDbCommand cmd5 = new OleDbCommand();
            OleDbCommand cmd6 = new OleDbCommand();
            OleDbCommand cmd7 = new OleDbCommand();
            OleDbCommand cmd8 = new OleDbCommand();

            OleDbCommand cmd9 = new OleDbCommand();
            OleDbCommand cmd10 = new OleDbCommand();
            OleDbCommand cmd11 = new OleDbCommand();
            OleDbCommand cmd12 = new OleDbCommand();

            OleDbCommand cmd13 = new OleDbCommand();

            cmd1.Connection = con;
            cmd2.Connection = con;
            cmd3.Connection = con;
            cmd4.Connection = con;

            cmd5.Connection = con;
            cmd6.Connection = con;
            cmd7.Connection = con;
            cmd8.Connection = con;

            cmd9.Connection = con;
            cmd10.Connection = con;
            cmd11.Connection = con;
            cmd12.Connection = con;

            cmd13.Connection = con;

            cmd1.CommandText = "select BackgA from myDefault";
            cmd2.CommandText = "select BackgR from myDefault";
            cmd3.CommandText = "select BackgG from myDefault";
            cmd4.CommandText = "select BackgB from myDefault";

            cmd5.CommandText = "select BallA from myDefault";
            cmd6.CommandText = "select BallR from myDefault";
            cmd7.CommandText = "select BallG from myDefault";
            cmd8.CommandText = "select BallB from myDefault";

            cmd9.CommandText = "select PlayerA from myDefault";
            cmd10.CommandText = "select PlayerR from myDefault";
            cmd11.CommandText = "select PlayerG from myDefault";
            cmd12.CommandText = "select PlayerB from myDefault";

            cmd13.CommandText = "select Speed from myDefault";

            try
            {
                int BackA = (int)cmd1.ExecuteScalar();
                int BackR = (int)cmd2.ExecuteScalar();
                int BackG = (int)cmd3.ExecuteScalar();
                int BackB = (int)cmd4.ExecuteScalar();

                int BallA = (int)cmd5.ExecuteScalar();
                int BallR = (int)cmd6.ExecuteScalar();
                int BallG = (int)cmd7.ExecuteScalar();
                int BallB = (int)cmd8.ExecuteScalar();

                int PlayerA = (int)cmd9.ExecuteScalar();
                int PlayerR = (int)cmd10.ExecuteScalar();
                int PlayerG = (int)cmd11.ExecuteScalar();
                int PlayerB = (int)cmd12.ExecuteScalar();

                int Speed = (int)cmd13.ExecuteScalar();

                panel1.BackColor = Color.FromArgb(BackA, BackR, BackG, BackB);
                panel2.BackColor = Color.FromArgb(BallA, BallR, BallG, BallB);
                panel3.BackColor = Color.FromArgb(PlayerA, PlayerR, PlayerG, PlayerB);
                label5.Text = Convert.ToString(Speed);

                if (Speed == 100)
                    rbDefault.Select();
                if (Speed == 50)
                    rbmx2.Select();
                if (Speed == 33)
                    rbmx3.Select();
                if (Speed == 25)
                    rbmx4.Select();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            con.Close();
        }



        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            con.Open();
            OleDbCommand cmd1 = new OleDbCommand();
            OleDbCommand cmd2 = new OleDbCommand();
            OleDbCommand cmd3 = new OleDbCommand();
            OleDbCommand cmd4 = new OleDbCommand();

            OleDbCommand cmd5 = new OleDbCommand();
            OleDbCommand cmd6 = new OleDbCommand();
            OleDbCommand cmd7 = new OleDbCommand();
            OleDbCommand cmd8 = new OleDbCommand();

            OleDbCommand cmd9 = new OleDbCommand();
            OleDbCommand cmd10 = new OleDbCommand();
            OleDbCommand cmd11 = new OleDbCommand();
            OleDbCommand cmd12 = new OleDbCommand();

            OleDbCommand cmd13 = new OleDbCommand();

            cmd1.Connection = con;
            cmd2.Connection = con;
            cmd3.Connection = con;
            cmd4.Connection = con;

            cmd5.Connection = con;
            cmd6.Connection = con;
            cmd7.Connection = con;
            cmd8.Connection = con;

            cmd9.Connection = con;
            cmd10.Connection = con;
            cmd11.Connection = con;
            cmd12.Connection = con;

            cmd13.Connection = con;

            cmd1.CommandText = "Update mySettings set BackgA ='" + panel1.BackColor.A + "';";
            cmd2.CommandText = "Update mySettings set BackgR ='" + panel1.BackColor.R + "';";
            cmd3.CommandText = "Update mySettings set BackgG ='" + panel1.BackColor.G + "';";
            cmd4.CommandText = "Update mySettings set BackgB ='" + panel1.BackColor.B + "';";

            cmd5.CommandText = "Update mySettings set BallA ='" + panel2.BackColor.A + "';";
            cmd6.CommandText = "Update mySettings set BallR ='" + panel2.BackColor.R + "';";
            cmd7.CommandText = "Update mySettings set BallG ='" + panel2.BackColor.G + "';";
            cmd8.CommandText = "Update mySettings set BallB ='" + panel2.BackColor.B + "';";

            cmd9.CommandText = "Update mySettings set PlayerA ='" + panel3.BackColor.A + "';";
            cmd10.CommandText = "Update mySettings set PlayerR ='" + panel3.BackColor.R + "';";
            cmd11.CommandText = "Update mySettings set PlayerG ='" + panel3.BackColor.G + "';";
            cmd12.CommandText = "Update mySettings set PlayerB ='" + panel3.BackColor.B + "';";

            cmd13.CommandText = "Update mySettings set Speed ='" + Convert.ToInt16(label5.Text) + "';";

            cmd1.ExecuteNonQuery();
            cmd2.ExecuteNonQuery();
            cmd3.ExecuteNonQuery();
            cmd4.ExecuteNonQuery();

            cmd5.ExecuteNonQuery();
            cmd6.ExecuteNonQuery();
            cmd7.ExecuteNonQuery();
            cmd8.ExecuteNonQuery();

            cmd9.ExecuteNonQuery();
            cmd10.ExecuteNonQuery();
            cmd11.ExecuteNonQuery();
            cmd12.ExecuteNonQuery();

            cmd13.ExecuteNonQuery();

            con.Close();


            this.Close();
        }

      
    }
}
