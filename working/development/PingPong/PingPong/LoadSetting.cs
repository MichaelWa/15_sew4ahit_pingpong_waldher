﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PingPong
{
    public class LoadSetting
    {
        public Color BackC { get; set; }
        public Color BallC { get; set; }
        public Color PlayerC { get; set; }
        public int Speed { get; set; }

        public LoadSetting(Color BackC, Color BallC, Color PlayerC, int Speed)
        {
            this.BackC = BackC;
            this.BallC = BallC;
            this.PlayerC = PlayerC;
            this.Speed = Speed;
        }
    }
}
