﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingPong
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void bGame_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form_Game fm = new Form_Game();
            fm.ShowDialog();
            this.Show();
        }

        private void bHighscore_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form_Highscore fm = new Form_Highscore();
            fm.ShowDialog();
            this.Show();
        }

        private void bSettings_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form_Settings fm = new Form_Settings();
            fm.ShowDialog();
            this.Show();
        }
    }
}
