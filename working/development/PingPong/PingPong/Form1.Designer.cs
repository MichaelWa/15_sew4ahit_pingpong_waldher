﻿namespace PingPong
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bGame = new System.Windows.Forms.Button();
            this.bHighscore = new System.Windows.Forms.Button();
            this.bSettings = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bGame
            // 
            this.bGame.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.bGame.Location = new System.Drawing.Point(82, 41);
            this.bGame.Name = "bGame";
            this.bGame.Size = new System.Drawing.Size(110, 37);
            this.bGame.TabIndex = 0;
            this.bGame.Text = "PLAY";
            this.bGame.UseVisualStyleBackColor = false;
            this.bGame.Click += new System.EventHandler(this.bGame_Click);
            // 
            // bHighscore
            // 
            this.bHighscore.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.bHighscore.Location = new System.Drawing.Point(82, 109);
            this.bHighscore.Name = "bHighscore";
            this.bHighscore.Size = new System.Drawing.Size(110, 38);
            this.bHighscore.TabIndex = 1;
            this.bHighscore.Text = "HIGHSCORE";
            this.bHighscore.UseVisualStyleBackColor = false;
            this.bHighscore.Click += new System.EventHandler(this.bHighscore_Click);
            // 
            // bSettings
            // 
            this.bSettings.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.bSettings.Location = new System.Drawing.Point(82, 176);
            this.bSettings.Name = "bSettings";
            this.bSettings.Size = new System.Drawing.Size(110, 37);
            this.bSettings.TabIndex = 2;
            this.bSettings.Text = "SETTINGS";
            this.bSettings.UseVisualStyleBackColor = false;
            this.bSettings.Click += new System.EventHandler(this.bSettings_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.bSettings);
            this.Controls.Add(this.bHighscore);
            this.Controls.Add(this.bGame);
            this.Name = "Form1";
            this.Text = "Menu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bGame;
        private System.Windows.Forms.Button bHighscore;
        private System.Windows.Forms.Button bSettings;
    }
}

