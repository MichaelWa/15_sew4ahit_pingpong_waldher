﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;

namespace PingPong
{
    public partial class Form_Game : Form
    {
        public static Paddle p1 = new Paddle();
        public static Paddle p2 = new Paddle();

        public static Ball myBall = new Ball();

        Graphics draw;

        string conStr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=mydb.accdb;";
        OleDbConnection con;

        public string filepath = "Highscore.txt";
        public int counter;
        public string line;
        int maxscore = 5;

        public Form_Game()
        {
            InitializeComponent();
            con = new OleDbConnection(conStr);
            LoadSetting setting = GetSetting();
            timer1.Interval =setting.Speed;
            timer1.Start();
        }
        private void Form_Game_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();
        }
        public LoadSetting GetSetting()
        {
 
            con.Open();

            OleDbCommand cmd1 = new OleDbCommand();
            OleDbCommand cmd2 = new OleDbCommand();
            OleDbCommand cmd3 = new OleDbCommand();
            OleDbCommand cmd4 = new OleDbCommand();

            OleDbCommand cmd5 = new OleDbCommand();
            OleDbCommand cmd6 = new OleDbCommand();
            OleDbCommand cmd7 = new OleDbCommand();
            OleDbCommand cmd8 = new OleDbCommand();

            OleDbCommand cmd9 = new OleDbCommand();
            OleDbCommand cmd10 = new OleDbCommand();
            OleDbCommand cmd11 = new OleDbCommand();
            OleDbCommand cmd12 = new OleDbCommand();

            OleDbCommand cmd13 = new OleDbCommand();

            cmd1.Connection = con;
            cmd2.Connection = con;
            cmd3.Connection = con;
            cmd4.Connection = con;

            cmd5.Connection = con;
            cmd6.Connection = con;
            cmd7.Connection = con;
            cmd8.Connection = con;

            cmd9.Connection = con;
            cmd10.Connection = con;
            cmd11.Connection = con;
            cmd12.Connection = con;

            cmd13.Connection = con;

            cmd1.CommandText = "select BackgA from mySettings";
            cmd2.CommandText = "select BackgR from mySettings";
            cmd3.CommandText = "select BackgG from mySettings";
            cmd4.CommandText = "select BackgB from mySettings";

            cmd5.CommandText = "select BallA from mySettings";
            cmd6.CommandText = "select BallR from mySettings";
            cmd7.CommandText = "select BallG from mySettings";
            cmd8.CommandText = "select BallB from mySettings";

            cmd9.CommandText = "select PlayerA from mySettings";
            cmd10.CommandText = "select PlayerR from mySettings";
            cmd11.CommandText = "select PlayerG from mySettings";
            cmd12.CommandText = "select PlayerB from mySettings";

            cmd13.CommandText = "select Speed from mySettings";
            LoadSetting ls = new LoadSetting(Color.Black,Color.White,Color.White,100);
            
            try
            {
                int BackA = (int)cmd1.ExecuteScalar();
                int BackR = (int)cmd2.ExecuteScalar();
                int BackG = (int)cmd3.ExecuteScalar();
                int BackB = (int)cmd4.ExecuteScalar();
                Color BackC = Color.FromArgb(BackA, BackR, BackG, BackB);

                int BallA = (int)cmd5.ExecuteScalar();
                int BallR = (int)cmd6.ExecuteScalar();
                int BallG = (int)cmd7.ExecuteScalar();
                int BallB = (int)cmd8.ExecuteScalar();
                Color BallC = Color.FromArgb(BallA, BallR, BallG, BallB);

                int PlayerA = (int)cmd9.ExecuteScalar();
                int PlayerR = (int)cmd10.ExecuteScalar();
                int PlayerG = (int)cmd11.ExecuteScalar();
                int PlayerB = (int)cmd12.ExecuteScalar();
                Color PlayerC = Color.FromArgb(PlayerA, PlayerR, PlayerG, PlayerB);

                int Speed = (int)cmd13.ExecuteScalar();

                con.Close();
                ls = new LoadSetting(BackC, BallC, PlayerC, Speed);


            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                con.Close();
            }
            return ls;
        }
        public void Draw(Graphics draw)
        {

            LoadSetting setting = GetSetting();
            Font font = new Font("Calibri", 100);

            draw.Clear(setting.BackC);

            draw.FillRectangle(new SolidBrush(setting.PlayerC), new Rectangle(p1.location, p1.size));
            draw.FillRectangle(new SolidBrush(setting.PlayerC), new Rectangle(p2.location, p2.size));

            draw.DrawString(p1.Score.ToString(), font, new SolidBrush(Color.White), new Point(this.Width / 2 - 400, 75));
            draw.DrawString(p2.Score.ToString(), font, new SolidBrush(Color.White), new Point(this.Width / 2 + 300, 75));

            draw.FillEllipse(new SolidBrush(setting.BallC), new Rectangle(myBall.location, myBall.size));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            LoadSetting setting = GetSetting();

            if (timer1.Interval == setting.Speed)
            {
                p1.location.X = 20;
                p1.location.Y = (this.Height - p1.size.Height) / 2;

                p2.location.X = (this.Width - p2.size.Width) - 20;

                pictureBox1.Size = new Size(this.Width, this.Height);
                pictureBox1.Refresh();
                draw = pictureBox1.CreateGraphics();

                myBall.location = new Point(this.Width / 2, this.Height / 2);
                timer1.Interval = setting.Speed/2;
            }


            Movement();
            Collisions();
            Draw(draw);
        }
        public static void Movement()
        {
            if (p1.moveUp == true)
                p1.location.Y -= 5;
            if (p1.moveDown == true)
                p1.location.Y += 5;

            if (p2.moveUp == true)
                p2.location.Y -= 5;
            if (p2.moveDown == true)
                p2.location.Y += 5;

            myBall.location.X += myBall.speed.X;
            myBall.location.Y += myBall.speed.Y;
        }
        private void Form_Game_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.W)
                p1.moveUp = true;
            if (e.KeyCode == Keys.S)
                p1.moveDown = true;
            if (e.KeyCode == Keys.Up)
                p2.moveUp = true;
            if (e.KeyCode == Keys.Down)
                p2.moveDown = true;

            if (e.KeyCode == Keys.Escape)
                Application.Exit();
        }
        private void Form_Game_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.W)
                p1.moveUp = false;
            if (e.KeyCode == Keys.S)
                p1.moveDown = false;

            if (e.KeyCode == Keys.Up)
                p2.moveUp = false;
            if (e.KeyCode == Keys.Down)
                p2.moveDown = false;

            if (e.KeyCode == Keys.Escape)
                Application.Exit();
        }
        

        public void Collisions()
        {
            if (p1.location.Y < 0)
                p1.location.Y = 0;
            if (p1.location.Y + p1.size.Height > this.Height-40)
                p1.location.Y = this.Height - p1.size.Height-40;

            if (p2.location.Y < 0)
                p2.location.Y = 0;
            if (p2.location.Y + p2.size.Height > this.Height-40)
                p2.location.Y = this.Height - p1.size.Height-40;

            if (myBall.location.Y < 0)
                myBall.speed.Y = -myBall.speed.Y;
            if (myBall.location.Y + myBall.size.Height > this.Height-40)
                myBall.speed.Y = -myBall.speed.Y;

            if (myBall.location.X < 0)
            {
                p2.Score++;
                if (p2.Score >= maxscore)
                {
                    ReadHighscore(filepath, p2.Score - p1.Score);
                    p1.Score = 0;
                    p2.Score = 0;
                    this.Close();
                }
                Reset();
            }
            if (myBall.location.X + myBall.size.Width > this.Width)
            {
                p1.Score++;
                if (p1.Score >= maxscore)
                {
                    ReadHighscore(filepath, p1.Score - p2.Score);
                    p1.Score = 0;
                    p2.Score = 0;
                    this.Close();
                }
                Reset(); 
            }

            if (new Rectangle(myBall.location, myBall.size).IntersectsWith(new Rectangle(p1.location, p1.size)) || new Rectangle(myBall.location, myBall.size).IntersectsWith(new Rectangle(p2.location, p2.size)))
                myBall.speed.X = -myBall.speed.X;
        }

        public void Reset()
        {

            myBall.location = new Point(this.Width / 2, this.Height / 2);
            myBall.speed.X = -myBall.speed.X;
            myBall.speed.Y = -myBall.speed.Y;
        }

        public void ReadHighscore(string filepath, int score)
        {
            if (File.Exists(filepath))
            {
                StreamReader sr = new StreamReader(filepath);
                List<int> mylist = new List<int>();
                int rank;

                while (sr.Peek() != -1)
                {
                    string[] line = (sr.ReadLine()).Split(';');
                    mylist.Add(Convert.ToInt16(line[1]));
                }
                while (sr.Peek() != -1)
                {
                    string[] line = (sr.ReadLine()).Split(';');
                    mylist.Add(Convert.ToInt16(line[1]));
                }
                sr.Close();
                foreach (int i in mylist)
                {
                    if (score > i)
                    {
                        rank = (mylist.IndexOf(i))+1;
                        WriteHighscore(filepath, score, rank, mylist);
                        break;
                    }
                }
                
            }
        }
        public void WriteHighscore(string filepath, int score,int rank, List<int> mylist)
        {
                StreamWriter sw = new StreamWriter(filepath, false);

                List<int> mylist2 = new List<int>();
                for (int i = 0; i < rank - 1; i++)
                {
                    mylist2.Add(mylist[i]);
                }
                mylist2.Add(score);
                for (int i = rank; i < 10; i++)
                {
                    mylist2.Add(mylist[i]);
                }
                for (int i = 0; i < 10; i++)
                {
                    sw.WriteLine((i+1) + ";" + mylist2[i]);
                }
                sw.Flush();
                sw.Close();
            }
        }
    }

